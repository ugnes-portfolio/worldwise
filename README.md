Desktop version only

# API boundaries

Due to API boundaries, initial city fetch and post might take about a minute. After that, app should work as expected

# Learning

- React Router, useNavigate. Store state in url
- createContext, useContext
- useEffect,useReducer, useCallback
- CSS modules

# How to use

- Press "Login" or "Start tracking now" and login with provided data
- Click on the city. You can change date, write notes and add city to the list
- The form does not appear if you click on sea or ocean.
- The "Countries" section shows the list of countries for your marked cities.
